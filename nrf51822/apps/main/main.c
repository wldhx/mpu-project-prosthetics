#include "app_util_platform.h"
#include "nordic_common.h"
#include "nrf_delay.h"
#include <stdbool.h>
#include <stdint.h>

#include "app_error.h"
#include "app_uart.h"

#include "nrf_drv_twi.h"

#include "simple_adv.h"
#include "simple_ble.h"

#include "nrf_drv_config.h"

#define SLAVE_ADDRESS 0x47

const nrf_drv_twi_t twi = NRF_DRV_TWI_INSTANCE(0);


// Intervals for advertising and connections
static simple_ble_config_t ble_config = {
    .platform_id = 0x90, // used as 4th octect in device BLE address
    .device_id = DEVICE_ID_DEFAULT,
    .adv_name = "хуй собачий", // used in advertisements if there is room
    .adv_interval = MSEC_TO_UNITS(500, UNIT_0_625_MS),
    .min_conn_interval = MSEC_TO_UNITS(500, UNIT_1_25_MS),
    .max_conn_interval = MSEC_TO_UNITS(1000, UNIT_1_25_MS)};

// service and characteristic handles
//  16-bit short uuid is 0x890f (bytes 12 and 13 of 128-bit UUID)
static simple_ble_service_t my_service = {
    // 829f8910-c556-11e6-a716-0242a47659ef
    .uuid128 = {{0xef, 0x59, 0x76, 0xa4, 0x42, 0x2, 0x16, 0xa7, 0xe6, 0x11,
                 0x56, 0xc5, 0xb5, 0xd8, 0x9f, 0x82}}};
static simple_ble_char_t my_char = {.uuid16 = 0x8910};
static uint16_t my_value = 0;

void turn_servo(uint8_t pin) {
    uint8_t a = (490 + my_value) >> 8;
    uint8_t b = (490 + my_value) & 0xFF;
    uint8_t tx_data[] = {pin, a, b};

    nrf_drv_twi_xfer_desc_t const xfer =
        NRF_DRV_TWI_XFER_DESC_TX(SLAVE_ADDRESS, tx_data, sizeof(tx_data));
    nrf_drv_twi_xfer(&twi, &xfer, NRF_DRV_TWI_FLAG_TX_NO_STOP);
}

// called automatically by simple_ble_init
void services_init(void) {
    // add my service
    simple_ble_add_service(&my_service);

    // add my characteristic
    simple_ble_add_characteristic(1, 1, 0, 16, // read, write, notify, vlen
                                  16, (uint8_t *)&my_value, &my_service,
                                  &my_char);
}

void ble_evt_write(ble_evt_t *p_ble_evt) {
    if (simple_ble_is_char_event(p_ble_evt, &my_char)) {
        printf("[BLE]: Char written");
        turn_servo(0);
    }
}

void uart_error_handle(app_uart_evt_t *p_event) {
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR) {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    } else if (p_event->evt_type == APP_UART_FIFO_ERROR) {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}

int main(void) {
    APP_ERROR_CHECK(nrf_drv_twi_init(&twi, NULL, NULL, NULL));
    nrf_drv_twi_enable(&twi);

    const app_uart_comm_params_t comm_params = {
        1, // RX pin
        2, // TX pin
        0,
        0,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200};

    {
        uint32_t err_code;

        APP_UART_FIFO_INIT(&comm_params,
                           256, // RX buffer size
                           256, // TX buffer size
                           uart_error_handle, APP_IRQ_PRIORITY_LOW, err_code);

        APP_ERROR_CHECK(err_code);
    }

    printf("[INIT]: start\n");

    // Setup BLE
    simple_ble_init(&ble_config);

    // Now register our advertisement configure functions
    simple_adv_only_name();

    printf("[INIT]: finish\n");

    // Enter main loop.
    while (1) {
        sd_app_evt_wait();
    }
}
