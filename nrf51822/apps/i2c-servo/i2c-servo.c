#include "app_util_platform.h"
#include "nordic_common.h"
#include "nrf_delay.h"
#include <stdbool.h>
#include <stdint.h>

#include "app_error.h"
#include "nrf_drv_twi.h"

#include "nrf_drv_config.h"

#define SLAVE_ADDRESS 0x47

const nrf_drv_twi_t twi = NRF_DRV_TWI_INSTANCE(0);

void turn_servo(uint8_t pin) {
    for (uint32_t i = 0; i < 180; i++) {
        uint8_t a = (490 + i) >> 8;
        uint8_t b = (490 + i) & 0xFF;
        uint8_t tx_data[] = {pin, a, b};

        APP_ERROR_CHECK(nrf_drv_twi_tx(&twi, SLAVE_ADDRESS, tx_data,
                                       sizeof(tx_data), false));
        nrf_delay_ms(15);
    }
}

int main(void) {
    APP_ERROR_CHECK(nrf_drv_twi_init(&twi, NULL, NULL, NULL));
    nrf_drv_twi_enable(&twi);
    while (true) {
        turn_servo(5);
    }
}
