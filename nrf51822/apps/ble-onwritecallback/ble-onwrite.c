#include "app_util_platform.h"
#include "nordic_common.h"
#include <stdbool.h>
#include <stdint.h>

#include "app_error.h"
#include "app_uart.h"

#include "simple_adv.h"
#include "simple_ble.h"

// Intervals for advertising and connections
static simple_ble_config_t ble_config = {
    .platform_id = 0x90, // used as 4th octect in device BLE address
    .device_id = DEVICE_ID_DEFAULT,
    .adv_name = "хуй собачий", // used in advertisements if there is room
    .adv_interval = MSEC_TO_UNITS(500, UNIT_0_625_MS),
    .min_conn_interval = MSEC_TO_UNITS(500, UNIT_1_25_MS),
    .max_conn_interval = MSEC_TO_UNITS(1000, UNIT_1_25_MS)};

// service and characteristic handles
//  16-bit short uuid is 0x890f (bytes 12 and 13 of 128-bit UUID)
static simple_ble_service_t my_service = {
    // e35c8bac-a062-4e3f-856d-2cfa87f2f171
    .uuid128 = {{0x23, 0xb8, 0xb9, 0x60, 0x5e, 0xa4, 0xdb, 0xbe, 0xe6, 0x11,
                 0xfd, 0xbd, 0x5e, 0x2, 0xa9, 0x34}}};
static simple_ble_char_t my_char = {.uuid16 = 0x8910};
static uint16_t my_value = 0;

// called automatically by simple_ble_init
void services_init(void) {
    // add my service
    simple_ble_add_service(&my_service);

    // add my characteristic
    simple_ble_add_characteristic(1, 1, 1, 0, // read, write, notify, vlen
                                  1, (uint8_t *)&my_value, &my_service,
                                  &my_char);
}

void ble_evt_write(ble_evt_t *p_ble_evt) {
    if (simple_ble_is_char_event(p_ble_evt, &my_char)) {
        printf("[BLE]: Char written");
    }
}

void uart_error_handle(app_uart_evt_t *p_event) {
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR) {
        APP_ERROR_HANDLER(p_event->data.error_communication);
    } else if (p_event->evt_type == APP_UART_FIFO_ERROR) {
        APP_ERROR_HANDLER(p_event->data.error_code);
    }
}

int main(void) {
    uint32_t err_code;
    const app_uart_comm_params_t comm_params = {
        1, // RX pin
        2, // TX pin
        0,
        0,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200};

    APP_UART_FIFO_INIT(&comm_params,
                       256, // RX buffer size
                       256, // TX buffer size
                       uart_error_handle, APP_IRQ_PRIORITY_LOW, err_code);
    APP_ERROR_CHECK(err_code);

    printf("[INIT]: start\n");

    // Setup BLE
    simple_ble_init(&ble_config);

    // Now register our advertisement configure functions
    simple_adv_only_name();

    printf("[INIT]: finish\n");

    // Enter main loop.
    while (1) {
        sd_app_evt_wait();
    }
}
