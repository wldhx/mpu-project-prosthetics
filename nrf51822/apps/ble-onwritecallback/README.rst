ble-onwritecallback
===================

Advertises a BLE service with a single write-only characteristic and fires a callback when it is written to.
