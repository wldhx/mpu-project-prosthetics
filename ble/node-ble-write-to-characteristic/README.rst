node-ble-write-to-characteristic
================================

A node.js script based on `noble <https://github.com/sandeepmistry/noble>`_ which discovers a device with given MAC and a characteristic with given UUID and writes to it.
