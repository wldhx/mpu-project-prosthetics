const noble = require('noble');


noble.on('stateChange', state => {
    if (state === 'poweredOn') {
        noble.startScanning();
    } else {
        noble.stopScanning();
    }
});

noble.on('discover', peripheral => {
    console.log(peripheral.address, peripheral.name);
    if (peripheral.address === 'c0:98:e5:90:52:2f') {
        console.log('Connecting...');
        peripheral.connect(e => {
                           console.log('Connected');
//                         peripheral.discoverAllServicesAndCharacteristics((e, s, c) => console.log(c))
                           peripheral.discoverSomeServicesAndCharacteristics([], ['829f8910c55611e6a7160242a47659ef'],
                                                                             (e, _, chars) => {
                                                                                 console.log(`Discovered chars: ${chars.map(c => c.uuid)}`);
                                                                                 chars.map((c, i, a) => c.write(Buffer.from([0]), false));
                                                                             });
        });
    }
});
